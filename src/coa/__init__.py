from .itpt import *
from . import itpt as itpt
# from . import coa_py as py
from . import cvt_go as go

def compile(in_p, out_p, lang = 'go',
            log_level = logging.DEBUG, log_path = 'coa-{}.log'.format(datetime.datetime.now().strftime('%Y-%m-%d %H-%M-%S'))):
  lang_config = {'go': {'grammar_p': './grammar.tx'}}
  debug = log_level in [logging.DEBUG]
  if lang in lang_config.keys():
    mm = metamodel_from_file(lang_config[lang]['grammar_p'], debug = debug)
    model = mm.model_from_file(in_p, debug = debug)
    inst = program(log_level = log_level, log_path = log_path)
    parsed = inst.itpt(model)
    splitter = {'go': go.go_program}
    source = splitter[lang](log_level, debug).cvt_go(parsed, in_p=in_p, out_p=out_p)
    with open(out_p, 'w') as file:
      file.write(source)
    return parsed, source
    # return parsed, 'not implemented yet'
  else:
    return ["error", "error"]