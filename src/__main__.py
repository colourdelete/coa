import argparse
import coa
import logging
import datetime
import sys


__desc__ = 'Compiles Coa to Go. To run directly using Python, navigate to "src" directory, and use "py ." instead.'

# # Set stdout to file
# sys.stdout = open('debug.stdout', 'w')
# sys.stderr = open('debug.stderr', 'w')

log_level = logging.DEBUG
log_path  = 'coa-{}.log'.format(datetime.datetime.now().strftime('%Y-%m-%d %H-%M-%S'))

rootLogger = logging.getLogger()
logging.basicConfig(
      format = '%(asctime)s %(levelname)-8s %(message)s',
      level = log_level,
      filename = log_path,
      datefmt = '%Y-%m-%d %H:%M:%S (%z)')
logFormatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s',
                                 datefmt = '%Y-%m-%d %H:%M:%S (%z)')

fileHandler = logging.FileHandler(log_path)
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler(sys.stdout)
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)

logging.debug('Logging ready.')
arg_parser = argparse.ArgumentParser(prog = 'Coa',
                                     description = __desc__)

arg_parser.add_argument('-i', '-s', '--source',
                        type = str,
                        required = True,
                        help = 'Coa source code path.')

arg_parser.add_argument('-o', '--out',
                        type = str,
                        required = True,
                        help = 'Coa source code path.')

logging.debug('Parsing args...')
args = arg_parser.parse_args()
logging.debug('Parsed args.')
spath = args.source
opath = args.out
logging.debug('Compiling...')
out = coa.compile(spath, opath, 'go', log_level = log_level, log_path = log_path)
logging.debug('Compiled.')

logging.info('Compilation successful. Use "go run" or "go build" to run/compile the program.')

import json
# print(json.dumps(out[0], indent=2))
with open('coa_out.json', 'w') as file:
  json.dump(out[0], file, indent=2)

# # Cleanup
# sys.stdout.close()
# sys.stderr.close()