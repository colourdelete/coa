# Example Pod

This is a example pod of a (fictional) server. This will not go over the source files.

## Structure

```
server.pod
├─license.md
├─readme.md
├─info.cod
├─docs
| ├─index.md
| ├─api_reference.md
| ├─pros_and_cons.md
| ├─tutorials.md
| └─notes.md
├─source
| ├─main.coa
| ├─dispatcher.coa
| ├─handler.coa
| └─manager.coa
```

## `license.md`

```
LICENSE TEXT (GPL, Apache, MIT, etc)
```

## `readme.md`/`README.md`

```
# Server

A random server example for Coa.
```

## `info.cod`

```coa
(
  pod.=(
    name.=(
      'dev.coa.side.examples.server',
    ),
    uuid.=(
      'c9206d24-96b0-4ea4-af30-ad94897ab605',
    ),
    desc.=(
      'A random server example for Coa.',
    ),
    ver.=(
      '1.2.3-Δ',
      name.=(
        'Trusty Tool',
      ),
    ),
    index.=(
      'dev.coa.index',
    ),
    source.=(
      'dev.coa.index/source/dev.coa.side.example.server',
    ),
  ),
),
```

## `docs/index.md`/`docs/README.md`

```
# Server Docs

Documentation work in progress.
```

## `docs/*`

```
# Server Docs

...
```
