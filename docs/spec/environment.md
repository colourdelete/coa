# Environment

Environments are also like [pods](/spec/pods.md), where there are three main directories: `/info`, `/docs`, and `/source`.

[Tip for drawing boxes and trees in Unicode (Wikipedia)](https://en.wikipedia.org/wiki/List_of_Unicode_characters#Box_Drawing)

The example below runs a (fictional) server.

```
coenv
├─info
│ ├─main.cod
│ ├─permits.cod
│ ├─dependencies.cod
│ ├─notes.cod
│ ├─legal.cod
│ └─readme.md
├─docs
│ ├─index.md
│ ├─api_reference.md
│ ├─pros_and_cons.md
│ ├─tutorials.md
│ └─notes.md
├─source
│ ├─main.coa
│ └─.test
│   ├─main.coa
│   └─timer.coa
└─cache
  └─server.pod
    ├─license.md
    ├─readme.md
    ├─info.cod
    ├─docs
    | ├─index.md
    | ├─api_reference.md
    | ├─pros_and_cons.md
    | ├─tutorials.md
    | └─notes.md
    └─source
      ├─main.coa
      ├─dispatcher.coa
      ├─handler.coa
      └─manager.coa
```

Git and the Git logo are either registered trademarks or trademarks of Software Freedom Conservancy, Inc., corporate home of the Git Project, in the United States and/or other countries.
