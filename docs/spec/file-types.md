# File Types

| Extension     | Name                   | Description                                                  |
| ------------- | ---------------------- | ------------------------------------------------------------ |
| `.coa`        | Coa Program            | Executable Coa file. `.cop` alias.                           |
| `.cod`        | Coa Data               | Executable Coa file. Cannot invoke non-simple functions.     |
| `.pod`        | Coa Pod                | Executable (compressed) Coa Pod.                             |
| `.tar.gz.pod` | Coa Compressed Pod     | Executable (compressed) Coa Pod. This example uses `.tar.gz` |
