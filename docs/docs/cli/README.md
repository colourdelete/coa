# Coa Command-Line Interface

The `coa` command manages your programs and projects. 

___

[Behold, the power of `coa`:](https://requests.readthedocs.io/en/master/)

```
user@machine:~/projects/hako_dialogs$ coa diagnose
Coa Diagnosis:
! Go Tools - used to compile Coa programs
  i Version 1.13.0
  ! Version 1.14.3 is available: download it from:
    https://golang.org
✓ Python Tools - used to compile Coa programs
  i Version 3.8.3 (latest version)
✓ Coa Kit - used to develop Coa programs
  i Version 0.0.0 (latest version)
✘ Project "Hako GUI Dialogs Example" Dependencies
  ✘ Missing: dev.coa.hako.widgets.dialogs, and 2 others
  i Fix using `coa pod set`
user@machine:~/projects/hako_dialogs$ pod set
Side Note: This is not static!
┌─███████░░░ dev.coa.hako.widgets.dialogs
├─██████░░░░ dev.coa.hako.widgets.windows
├─███████░░░ dev.coa.hako.widgets.scrolls
├─██████░░░░ dev.coa.hako.widgets.buttons
├─████░░░░░░ dev.coa.hako.widgets.styles
├─████████░░ dev.coa.hako.core
└─██░░░░░░░░ dev.coa.hako.bindings.side
┌─██████████ dev.coa.examples.hako.dialogs 
├─█████████░ dev.coa.core.io.streams
└─███████░░░ dev.coa.core.logging
user@machine:~/projects/gui_hello$ pod run
Side Note: This doesn't actually happen!
╭─Coa Dialog Example───────────────────────˅˄×╮
│                                             │
│ [Password˅]  [dev.coa.hako.dialogs.input]  ˄│
│ [Yes˅] [No˅] [dev.coa.hako.dialogs.choice] ╹│
│ [Password˅]  [dev.coa.hako.dialogs.colour] ˅│
│                                             │
╰─────────────────────────────────────────────╯
```