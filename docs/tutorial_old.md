# [OLD] Tutorial

This tutorial teaches the basics of Coa by example.

## Function Call

```
(
  .print('Hello, world!',),
)
```
`Object(Arguments)`

*Object* is the callee of the call. *Arguments* is the argument for the callee, and is a *Container*.

## Define Call

```
(
  site_url = 'https://example.com',
)
```
`Object = Object`

## Object

```
(
  true,
  1.1,
  1,
  'one',
  .print,
  ().(.print('Function evoked.',),),
)
```
`Object`

## Control Flow (If, Else, ...)

```
(
  
  .if(