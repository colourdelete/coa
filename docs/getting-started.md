# Getting Started

This guide helps you start coding in Coa quickly. Coa is easy to learn but is different from other programming languages such as Python, Go, and Rust.

## Setting Up

1. Install the following:
   1. Python 3.8+
   2. Go 1.14+
   3. (Optional) A Go IDE
   4. (Optional) A Python IDE
2. Download the latest release of Coa
3. (Optional) Add `coa/src` to `PATH`
4. Test it:
   1. Navigate to `coa/docs/examples`
   2. Run the example `hello.coa`.
   3. If the program outputs
      ```coa
      Hello. This is your environment now.
      [✓] Coa
        [✓] Coa                    0.1      (0.1+)
        [✓] Coa Standard Libraries 0.1      (0.1+)
        [✓] Coa Library Manager    0.1      (0.1+)
        [✓] PATH
      [✓] Environment
        [✓] Python                 3.8      (3.8+)
        [✓] Go                     1.14     (1.14+)
        [✓] PyCharm                2020.1   (2020+)
        [✓] GoLand                 2020.1.1 (2020+)
      ```
      , the setup process is complete.

## First Program

Again, Coa is simple, and easy to learn, but also different from other languages. People familiar with languages different from Coa may cringe a little.

```coa
(
  .print('Hello, world!'),
  .print('This is actually compiled to Go, then compiled and/or run.'),
)
```

This program outputs:

```
Hello, world!
This is actually compiled to Go, then compiled and/or run.
```
