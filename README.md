# Coa

Clean, objective, asynchronous.

## This is Work In Progress

## What is Coa?

Coa is a multi-paradigm programming language that supports easy asynchronization. Coa code is compiled/transpiled to Go, then built/run.
```coa
(
  through.=(('a', 'e', 'i', 'o', 'u', ' ', ), ), 
  translate = (input, ).(
    result.=('', ), 
    .for(.each(input, ), (i, char, ).(
      .if(char.in(through, ), ().(
        result.+=(char, ), 
      ), )
      .else(().(
        result.+=('"char"o"char"', ), 
      ), )
    ), ), 
    return(result, ), 
  )
  .print('Rovarspraket Translator', ), 
  input.=(.scan('Rovarspraket: ', ), ), 
  output.=(translate(input, ), ), 
  .print('English:      "output"', ), 
), 
```
```
Rovarspraket Translator
Roverspraket: Coa
English:      Cocoa
```

## How does Coa work?

A compiler converts Coa code to another programming language (Go for the default), which in turn compiles it to machine language. Some features may be unavailable or slow depending on the compiler. This compiler (this repo) converts Coa code into Go code using a Python program. It does not run commands such as `go run` or `go build` as of March 2020.

```
  Coa Code
     |     (Python program)
     v
  Go  Code
     |     (Go compiler)
     v
Machine Code
```